[Jupyter Notebook](notebooks/Cleaning_Log.ipynb)

This notebook documents my code for all my data cleaning and data processing as well as merging and the outputs of dataframe.describe(), dataframe.info() and the code answering the posed question.

However, the processed datasheets where created and exported in different files using Visual Studio Code.
