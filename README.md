### Repository for CA273 Assignment 3: Data Processing & Cleaning

* Summary of my work(100 words):

I have successfully managed to clean and process 6 datasheets (.xlsx, .csv and .json files). I have downloaded every file from Github onto my local machine. I used Visual Studio Code to experiment with cleaning using python commands, and to clean, process and export the files. The final code I used was recorded in a Google Colab Jupyter Notebook. The internet proved to be very useful in finding commands, debugging and searching for solutions. I had issues with Parse Error (Tokenizing Data) that made it impossible to import the datafiles from links on Google Colab and that's why I did all my coding on Visual Studio Code offline. Searching for a solution to this error wasted numerous days for me. Unfortunately I ran out of time to start cleaning the 2008.ods file.

* List of tools I used:

**1.** Google Colab as my Jupyter Notebook

**2.** Visual Studio Code to create, process and export final datasets

**3.** Microsoft Excel to view files for obvious mistakes

**4.** Notepad to view files for obvious mistakes

**5.** Git Bash to submit my code


* Links to my final dataset files:

1. [data_processed_a3-2010.json (as csv)](data/processed/data_processed_a3-2010.csv)

2. [data_processed_a3-2019-jan-jun.xlsx (as csv)](data/processed/data_processed_a3-2019-jan-jun.csv)

3. [data_processed_a3-2019-Jul-Dec.xlsx (as csv)](data/processed/data_processed_a3-2019-Jul-Dec.csv)

4. [data_processed_a3-2020-jan-jun.csv](data/processed/data_processed_a3-2020-jan-jun.csv)

5. [data_processed_a3-2020-jul-dec.csv](data/processed/data_processed_a3-2020-jul-dec.csv)

6. [data_processed_a3-2021-jan-sep.csv](data/processed/data_processed_a3-2021-jan-sep.csv)

* Link to my Jupyter Notebook:

[Jupyter Notebook](notebooks/Cleaning_Log.ipynb)
